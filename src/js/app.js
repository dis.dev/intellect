"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import HystModal from 'hystmodal';
import Tabs from "./components/tabs.js";
import Select from './components/select.js'; // Select plugin
import SimpleBar from 'simplebar'; // Кастомный скролл


// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Сворачиваемые блоки
collapse();

// Tabs
Tabs();

Select();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Modal HystModal
const myModal = new HystModal({
    linkAttributeName: 'data-hystmodal',
});

function getMobileOperatingSystem() {
    let userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (/android/i.test(userAgent)) {
        return "Android";
    }
    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "iOS";
    }
    return "unknown";
}

// form

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-form-send]')) {
        myModal.open('[data-success]')
    }
})

window.addEventListener("load", function (e) {

    if (getMobileOperatingSystem() === 'iOS') {
        document.querySelector('html').classList.add('device-ios')
    }
    else if (getMobileOperatingSystem() === 'Android') {
        document.querySelector('html').classList.add('device-android')
    }

    if (document.querySelectorAll('[data-simplebar]').length) {
        document.querySelectorAll('[data-simplebar]').forEach(scrollBlock => {
            new SimpleBar(scrollBlock, {
                autoHide: false
            });
        });
    }
});




