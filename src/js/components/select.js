export default () => {
    const selectArray = document.querySelectorAll('[data-select]');

    if (selectArray.length > 0) {

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-select-toggle]')) {
                let select = event.target.closest('[data-select]');
                if (select.classList.contains('open')) {
                    select.classList.remove('open');
                }
                else {
                    selectClose()
                    select.classList.add('open');
                }
            }
            else if (event.target.closest('[data-select-item]')) {
                const selectContainer = event.target.closest('[data-select]')
                let selectItem = event.target.closest('[data-select-item]').dataset.selectItem

                selectContainer.querySelector('[data-select-value]').value = selectItem
                selectContainer.querySelector('[data-select-placeholder]').innerHTML = selectItem
                selectContainer.querySelectorAll('[data-select-item]').forEach(el => {
                    el.classList.remove('active');
                })
                event.target.closest('[data-select-item]').classList.add('active')
                selectClose();
            }

            else {
                if (!event.target.closest('[data-select-content]')) {
                    selectClose();
                }
            }

            function selectClose() {
                selectArray.forEach(el => {
                    el.classList.remove('open');
                });
            }
        })
    }
};
